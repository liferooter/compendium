using Adw, Gtk;

namespace Comp {
    [GtkTemplate(ui = "/io/gitlab/liferooter/Compendium/ui/TextView.ui")]
    class TextView : Bin {

        enum SyntaxElement {
            HEADER,
            ORDERED_LIST,
            ITALIC,
            BOLD,
            LINK,
            STRIKETHROUGH
        }

        [GtkChild] public unowned SourceView editor;

        public MarkdownBuffer buffer;

        private Markdown.SyntaxElement[] syntax_elements;

        public TextTagTable tags;

        construct {
            tags = new TextTagTable ();

            buffer = new MarkdownBuffer (tags);
            buffer.changed.connect_after (update_cb);
            buffer.mark_set.connect_after (update_cb);

            syntax_elements = {
                new Markdown.Header (this),
                new Markdown.OrderedList (this),
                new Markdown.Italic (this),
                new Markdown.Bold (this),
                new Markdown.Link (this),
                new Markdown.Strikethrough (this)
            };

            editor.set_buffer (buffer);
        }

        public void setup_actions (Gtk.Window window) {
            ActionEntry[] ACTIONS = {
                { "bold", () => syntax_elements[SyntaxElement.BOLD].toggle ()  },
                { "italic", () => syntax_elements[SyntaxElement.ITALIC].toggle () },
                { "strikethrough", () => syntax_elements[SyntaxElement.STRIKETHROUGH].toggle ()  },
            };

            var action_group = new SimpleActionGroup ();
            action_group.add_action_entries (ACTIONS, this);
            window.insert_action_group ("editor", action_group);
        }

        void update_cb () {

            {
                TextIter buffer_start, buffer_end;
                buffer.get_bounds (out buffer_start, out buffer_end);
                buffer.remove_all_tags (buffer_start, buffer_end);
            }

            var lines = buffer.text.split ("\n");
            if (lines.length == 0)
                lines = {""};

            int cursor_line;
            {
                TextIter cursor_iter;
                buffer.get_iter_at_mark (out cursor_iter, buffer.get_insert ());
                cursor_line = cursor_iter.get_line ();
            }

            for (var i = 0; i < buffer.get_line_count (); i++) {

                if (lines[i].length > 0) {

                    foreach (var syntax_element in syntax_elements)
                        if (syntax_element.highlight_in_line (i, lines[i]))
                            break;
                }
            }
        }
    }
}
