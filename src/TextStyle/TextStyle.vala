using Gtk;

namespace Comp.Markdown {
    public delegate void RangeCallback (MarkdownBuffer buffer, int line_index, int start, int end);

    public class TextStyle {
        private RangeCallback apply_style_callback;

        static TextIter get_iter (TextBuffer buffer, int line, int index) {
            TextIter iter;
            buffer.get_iter_at_line_index (out iter, line, index);

            return iter;
        }

        static bool show_raw (TextBuffer buffer, int line) {
            if (buffer.has_selection)
                return true;
            TextIter cursor;
            buffer.get_iter_at_mark (out cursor, buffer.get_insert ());
            return cursor.get_line () == line;
        }

        protected TextStyle (RangeCallback callback) {
            apply_style_callback = callback;
        }

        public TextStyle.from_tag (TextTag tag) {
            apply_style_callback = (buffer, line, start, end) => {
                var start_iter = get_iter (buffer, line, start);
                var end_iter = get_iter (buffer, line, end);
                buffer.apply_tag (tag, start_iter, end_iter);
            };
        }

        public TextStyle.from_tags (TextTag in_focus_tag,
                                    TextTag not_in_focus_tag)
        {
            apply_style_callback = (buffer, line, start, end) => {
                var tag = show_raw (buffer, line)
                    ? in_focus_tag
                    : not_in_focus_tag;
                var start_iter = get_iter (buffer, line, start);
                var end_iter = get_iter (buffer, line, end);
                buffer.apply_tag (tag, start_iter, end_iter);
            };
        }

        public TextStyle.from_replacement (string old, string replacement) {
            apply_style_callback = (buffer, line, start, end) => {
                if (show_raw (buffer, line))
                    return;

                var text = buffer.lines[line].text[start:end].replace (old, replacement);

                buffer.begin_internal_action ();

                var start_iter = get_iter (buffer, line, start);
                var end_iter = get_iter (buffer, line, end);

                buffer.delete (ref start_iter, ref end_iter);
                buffer.insert (ref start_iter, text, text.length);

                buffer.end_internal_action ();
            };
        }

        public void apply (MarkdownBuffer buffer, int line, int start, int end) {
            apply_style_callback (buffer, line, start, end);
        }
    }
}