using Gtk;

namespace Comp.Markdown {
    /**
     * Bold text syntax element.
     */
    [Version (since = "1.0")]
    class Bold : SyntaxElement {

        protected override Regex REGEX {
            get;
            default = /(?|(?<=[^*]|^)(?<prefix>\*{2,3})[^*]+(?<postfix>\1)(?=[^*]|$)|(?<=[^_]|^)(?<prefix>_{2,3})[^_]+(?<postfix>\1)(?=[^_]|$))/;
        }

        protected override TextTag TAG {
            get;
            default = new TextTag ("bold") {
                weight = Pango.Weight.BOLD
            };
        }

        protected override string toggle_on_text (string text) {
            if (/^(\*|_)\1.*\1\1$/.match (text))
                return text[2:-2];
            else
                return "**%s**".printf (text);
        }

        protected override void toggle_around_cursor () {
            TextIter start, end;
            buffer.get_iter_at_offset (out start, buffer.cursor_position - 2);
            buffer.get_iter_at_offset (out end, buffer.cursor_position + 2);
            var str = buffer.get_slice (start, end, true);
            if (str == "****" || str == "____") {
                buffer.delete_range (start, end);
            } else {
                TextIter cursor_iter;
                buffer.insert_at_cursor ("****", 4);
                buffer.get_iter_at_offset (out cursor_iter, buffer.cursor_position - 2);
                buffer.place_cursor (cursor_iter);
            }
        }

        public Bold (Comp.TextView editor) {
            Object (
                editor: editor
            );
        }
    }
}