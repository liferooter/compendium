using Gtk;

namespace Comp.Markdown {
    /**
     * Italic text syntax element.
     */
    [Version (since = "1.0")]
    class Italic : SyntaxElement {

        protected override Regex REGEX {
            get;
            default = /(?|(?<=[^*]|^)(?<prefix>\*(\*\*)?)[^*]*(?<postfix>\1)(?=[^*]|$)|(?<=[^_]|^)(?<prefix>_(__)?)[^_]*(?<postfix>\1)(?=[^_]|$))/;
        }

        protected override TextTag TAG {
            get;
            default = new TextTag ("italic") {
                style = Pango.Style.ITALIC
            };
        }

        protected override string toggle_on_text (string text) {
            if (/^(\*|\*(\*{2})+)[^*].*\1$/.match (text)
                || /^(_|_(_{2})+)[^_].*\1$/.match (text))
                return text[1:-1];
            else
                return "_%s_".printf (text);
        }

        protected override void toggle_around_cursor () {
            TextIter cursor;
            buffer.get_iter_at_mark (out cursor, buffer.get_insert ());
            var lines = buffer.text.split ("\n");
            if (lines.length == 0)
                lines = {""};
            var line = lines[cursor.get_line ()];

            var left_str = line[int.max (cursor.get_line_index () - 3, 0):int.min(cursor.get_line_index (), line.length)];
            var right_str = line[int.min (cursor.get_line_index (), line.length):int.min(cursor.get_line_index () + 3, line.length)];

            MatchInfo left_match, right_match;
            var prefix_length = 0;
            var prefix_char = "";

            if (/([_*])\1*$/.match (left_str, 0, out left_match)) {
                prefix_char = left_match.fetch (1) ?? "";
                prefix_length = left_match.fetch (0)?.length ?? 0;
            } if (/^([_*])\1*/.match (right_str, 0, out right_match)) {
                var postfix_char = right_match.fetch (1) ?? "";
                var postfix_length = right_match.fetch (0)?.length ?? 0;
                if (postfix_char == prefix_char && postfix_length < prefix_length)
                    prefix_length = postfix_length;
            } else prefix_length = 0;

            TextIter start, end;
            if (prefix_length > 0 && prefix_length != 2) {
                buffer.get_iter_at_offset (out start, buffer.cursor_position - 1);
                buffer.get_iter_at_offset (out end, buffer.cursor_position + 1);
                buffer.delete_range (start, end);
            } else {
                TextIter cursor_iter;
                buffer.insert_at_cursor ("__", 2);
                buffer.get_iter_at_offset (out cursor_iter, buffer.cursor_position - 1);
                buffer.place_cursor (cursor_iter);
            }
        }

        public Italic (Comp.TextView editor) {
            Object (
                editor: editor
            );
        }
    }
}