using Gtk;

namespace Comp.Markdown {
    /**
     * Syntax element.
     *
     * Base class of Markdown syntax element.
     */
    [Version (since = "1.0")]
    abstract class SyntaxElement : Object {

        /**
         * Main text tag of syntax element.
         */
        [Version (since = "1.0")]
        protected virtual TextTag TAG {
            get;
            default = new TextTag (null);
        }

        /**
         * Regular expression for matching syntax element.
         */
        [Version (since = "1.0")]
        protected virtual Regex REGEX {
            get;
            default = /^^/; // Never-matching regex
        }

        /**
         * Text tag used to hide non-renderable syntax elements.
         *
         * Used when source is not shown.
         */
        private static TextTag INVISIBLE_TAG = new TextTag ("invisible") {
            invisible = true
        };

        /**
         * Text tag used to dim non-renderable syntax elements
         *
         * Used when source is shown.
         */
        private static TextTag TRANSPARENT_TAG = new TextTag ("transparent") {
            foreground_rgba = { 0, 0, 0, 0.3f }
        };

        construct {
            if (editor.tags.lookup ("invisible") == null)
                editor.tags.add (INVISIBLE_TAG);
            if (editor.tags.lookup ("transparent") == null)
                editor.tags.add (TRANSPARENT_TAG);
            editor.tags.add (TAG);
        }

        /**
         * Compendium text view which uses the syntax element.
         *
         * Must be set in the constructor.
         */
        [Version (since = "1.0")]
        public Comp.TextView editor { get; construct; }

        /**
         * Text view which uses the syntax element.
         *
         * Auxiliary property.
         */
        [Version (since = "1.0")]
        public Gtk.TextView textview { get { return editor.editor; } }

        /**
         * Text buffer which uses the syntax element.
         *
         * Auxiliary property.
         */
        [Version (since = "1.0")]
        protected TextBuffer buffer { get { return textview.buffer; } }

        /**
         * Text buffer's cursor as text iterator.
         *
         * Auxiliary property.
         */
        [Version (since = "1.0")]
        protected TextIter cursor {
            get {
                TextIter cursor_iter;
                buffer.get_iter_at_mark (out cursor_iter, buffer.get_insert ());
                return cursor_iter;
            } set {
                buffer.place_cursor (value);
            }
        }

        /**
         * Text lines of the buffer.
         *
         * Auxiliary property.
         */
        [Version (since = "1.0")]
        protected string[] lines {
            owned get {
                TextIter start, end;
                buffer.get_bounds (out start, out end);
                var _lines = buffer.get_text (start, end, true).split ("\n");

                if (_lines.length == 0)
                    _lines = {""};


                return _lines;
            }
        }

        /**
         * Highligh all occurences of the syntax element in the line.
         *
         * Default implementation hides prefix and postfix
         * and highlighs body with default tag.
         *
         * @param line the line to highlight all syntax element occurences in.
         * @param line_text text of the line
         *
         * @return whether to skip all syntax element checks for this line.
         */
        [Version (since = "1.0")]
        public virtual bool highlight_in_line (int line, string line_text) {

            int min_pos = 0;
            MatchInfo match;
            try {
                while (REGEX.match_full (
                    line_text,
                    -1,
                    min_pos,
                    0,
                    out match
                )) {
                    int start, post_prefix, pre_postfix, end;
                    match.fetch_named_pos ("prefix", out start, out post_prefix);
                    match.fetch_named_pos ("postfix", out pre_postfix, out end);

                    highlight_hidden (line, start, post_prefix);
                    highlight_range (line, post_prefix, pre_postfix);
                    highlight_hidden (line, pre_postfix, end);

                    min_pos = end;
                }
            } catch (RegexError e) {
                critical ("Can't highlight: %s", e.message);
            }

            return false;
        }

        /**
         * Whether to show source code of the line.
         *
         * Mainly to choose between hiding and dimming
         * of non-renderable syntax elements.
         * Source code is shown when the line is in focus
         * or user selects text.
         *
         * @param line the line to find out whether to show source for.
         *
         * @return whether to show source code of the line
         */
        [Version (since = "1.0")]
        protected bool is_show_source_mode (uint line) {
            return buffer.has_selection || cursor.get_line () == line;
        }

        /**
         * Apply text tag for text range defined by line indexes.
         *
         * Auxiliary method.
         *
         * @param tag tag to apply
         * @param line line which contains the range
         * @param start index of the start of the range in line, in bytes
         * @param end index of the end of the range in line, in bytes
         */
        [Version (since = "1.0")]
        protected void apply_tag_for_range (TextTag tag, int line, int start, int end) {
            TextIter start_iter, end_iter;
            buffer.get_iter_at_line_index (out start_iter, line, start);
            buffer.get_iter_at_line_index (out end_iter, line, end);
            buffer.apply_tag (tag, start_iter, end_iter);
        }

        /**
         * Highlight text range with default text tag.
         *
         * Auxiliary method.
         *
         * @param line line which contains the range
         * @param start index of the start of the range in line, in bytes
         * @param end index of the end of the range in line, in bytes
         */
        [Version (since = "1.0")]
        protected void highlight_range (int line, int start, int end) {
            apply_tag_for_range ((!) TAG, line, start, end);
        }

        /**
         * Dim or hide text range depending on whether show source mode is enabled.
         *
         * Auxiliary method.
         *
         * @param line line which contains the range
         * @param start index of the start of the range in line, in bytes
         * @param end index of the end of the range in line, in bytes
         */
        [Version (since = "1.0")]
        protected void highlight_hidden (int line, int start, int end) {
            var hidden_tag = is_show_source_mode (line)
                ? TRANSPARENT_TAG
                : INVISIBLE_TAG;
            apply_tag_for_range (hidden_tag, line, start, end);
        }

        /**
         * Toggle syntax element on text.
         *
         * Replace selected text with output of `toggle_on_text`.
         * If not selected, call `toggle_around_cursor`.
         *
         * @param buffer text buffer
         */
        [Version (since = "1.0")]
        public void toggle () {
            if (!buffer.has_selection) {

                buffer.begin_user_action ();
                toggle_around_cursor ();
                buffer.end_user_action ();

                return;
            }

            TextIter start, end;
            buffer.get_selection_bounds (out start, out end);
            var start_offset = start.get_offset ();

            var text = buffer.get_slice (start, end, true);

            buffer.begin_user_action ();

            text = toggle_on_text (text);

            buffer.delete_selection (false, true);

            buffer.get_iter_at_offset (out start, start_offset);

            buffer.insert (ref start, text, -1);

            buffer.get_iter_at_offset (out start, start_offset);
            buffer.get_iter_at_offset (out end, start_offset + text.char_count ());

            buffer.select_range (start, end);

            buffer.end_user_action ();
        }

        /**
         * Get string width in text view.
         *
         * @param str string
         *
         * @return string's width in text view, in pixels
         */
        protected int get_chars_width (string str) {
            int x;

            var layout = editor.create_pango_layout (str);
            layout.get_pixel_size (out x, null);

            return x;
        }

        /**
         * Toggle syntax element on text.
         *
         * @param text text
         *
         * @return text where the syntax element was toggled
         */
        [Version (since = "1.0")]
        protected virtual string toggle_on_text (string text) {
            return text;
        }

        /**
         * Toggle syntax element around the cursor.
         */
        [Version (since = "1.0")]
        protected virtual void toggle_around_cursor () {}
    }
}