using Gtk;

namespace Comp.Markdown {
    /**
     * Ordered list syntax element.
     */
    [Version (since = "1.0")]
    class OrderedList : SyntaxElement {
        protected override Regex REGEX {
            get;
            default = /\d+[.)]\s/;
        }

        protected override TextTag TAG {
            get;
            default = new TextTag ("ordered-list") {
                indent = 16
            };
        }

        public OrderedList (Comp.TextView editor) {
            Object (
                editor: editor
            );
        }

        public override bool highlight_in_line (int line, string line_text) {
            MatchInfo match;
            if (REGEX.match (line_text, 0, out match)) {
                int start, end;
                match.fetch_pos (0, out start, out end);
                highlight_range (line, start, end);
                apply_tag_for_range (
                    buffer.create_tag (
                        null,
                        "left-margin", textview.left_margin - get_chars_width ((!) match.fetch (0)) + 16,
                        null
                    ),
                    line,
                    start,
                    end
                );
            }

            return false;
        }

    }
}