using Gtk;

namespace Comp.Markdown {
    /**
     * Strikethrough text syntax element.
     */
    [Version (since = "1.0")]
    class Strikethrough : SyntaxElement {

        protected override Regex REGEX {
            get;
            default = /(?<=[^~]|^)(?<prefix>~~)[^~]*(?<postfix>~~)(?=[^~]|$)/;
        }

        protected override TextTag TAG {
            get;
            default = new TextTag ("strikethrough") {
                strikethrough = true
            };
        }

        protected override string toggle_on_text (string text) {
            if (/^~~.*~~$/.match (text))
                return text[2:-2];
            else if (/^~.*~$/.match (text))
                return text[1:-1];
            else
                return "~~%s~~".printf (text);
        }

        protected override void toggle_around_cursor () {
            TextIter start, end;
            buffer.get_iter_at_offset (out start, buffer.cursor_position - 2);
            buffer.get_iter_at_offset (out end, buffer.cursor_position + 2);
            var str = buffer.get_slice (start, end, true);
            if (str == "~~~~") {
                buffer.delete_range (start, end);
            } else {
                TextIter cursor_iter;
                buffer.insert_at_cursor ("~~~~", 4);
                buffer.get_iter_at_offset (out cursor_iter, buffer.cursor_position - 2);
                buffer.place_cursor (cursor_iter);
            }
        }

        public Strikethrough (Comp.TextView editor) {
            Object (
                editor: editor
            );
        }
    }
}