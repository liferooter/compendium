using Gtk;

namespace Comp.Markdown {
    class Header : SyntaxElement {

        protected override Regex REGEX {
            get;
            default = /^(#{1,4})(\s.*)/;
        }

        /**
         * Text tags for all header levels
         */
        private TextTag[] HEADER_TAGS = {
            new TextTag ("h1") {
                size_points = 20,
                weight = 800,
            },
            new TextTag ("h2") {
                size_points = 15,
                weight = 800,
            },
            new TextTag ("h3") {
                size_points = 15,
                weight = 700,
            },
            new TextTag ("h4") {
                size_points = 13,
                weight = 700,
            }
        };

        public Header (Comp.TextView editor) {
            Object (
                editor: editor
            );
        }

        construct {
            // Add header text tags to the tag table
            foreach (var tag in HEADER_TAGS)
                editor.tags.add (tag);
        }

        public override bool highlight_in_line (int line, string line_text) {
            MatchInfo match;

            if (REGEX.match (line_text, 0, out match)) {
                int header_level, end;

                // Calculate header level by number of hash signs
                var prefix = match.fetch (1) ?? "";
                header_level = prefix.length;

                match.fetch_pos (0, null, out end);

                // Hide hash signs
                highlight_hidden (line, 0, header_level);
                // Apply spacing
                apply_tag_for_range (
                    get_line_spacing_tag (
                        line == 0
                            ? 0
                            : header_level == 1
                                ? 30
                                : 10,
                        20
                    ),
                    line,
                    0,
                    header_level
                );
                // Apply indentation to counteract indentation by hash signs
                var indent_size = get_chars_width ((is_show_source_mode (line) ? prefix : "") + " ");
                apply_tag_for_range (
                    buffer.create_tag (
                        null,
                        "left-margin", textview.left_margin - indent_size,
                        "indent", -indent_size,
                        null
                    ),
                    line,
                    0,
                    header_level
                );

                // Apply header tag
                apply_tag_for_range (
                    HEADER_TAGS[header_level - 1],
                    line,
                    header_level + 1,
                    end
                );

                // Skip syntax parsing for header
                return true;
            }

            return false;
        }

        /**
         * Get text tag for given spacing above and below paragraph.
         *
         * @param above spacing above paragraph
         * @param below spacing below paragraph
         */
        TextTag get_line_spacing_tag (int above, int below) {
            return buffer.create_tag (
                null,
                "pixels-above-lines", above,
                "pixels-below-lines", below,
                null
            );
        }
    }
}