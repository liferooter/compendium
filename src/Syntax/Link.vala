using Gtk;

namespace Comp.Markdown {
    /**
     * Link text syntax element.
     */
    [Version (since = "1.0")]
    class Link : SyntaxElement {

        protected override Regex REGEX {
            get;
            default = /(?<=\s|^)(?|(?<prefix>)https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()!@:%_\+.~#?&\/\/=]*)(?<postfix>)|(?<prefix>\[)([^\[]+)()(?<postfix>\]\([^)]+\)))(?=\s|$)/;
        }

        protected override TextTag TAG {
            get;
            default = new TextTag ("link") {
                underline = Pango.Underline.SINGLE,
                foreground = "blue"
            };
        }

        public Link (Comp.TextView editor) {
            Object (
                editor: editor
            );
        }
    }
}