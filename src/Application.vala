using Adw, Gtk;

namespace Comp {
    class Application : Adw.Application {
        struct AccelEntry {
            string action_name;
            string[] accels;
        }

        static ActionEntry[] ACTIONS = {
            { "quit", action_quit }
        };

        static AccelEntry[] ACCELS = {
            { "app.quit", {"<Control>q"} },
            { "window.close", { "<Control>w" } },
            { "editor.bold", {"<Control>b"} },
            { "editor.italic", {"<Control>i"} },
            { "editor.strikethrough", {"<Control><Shift>x"} }
        };

        public static GLib.Settings settings;

        public Application () {
            Object (
                flags: ApplicationFlags.FLAGS_NONE,
                application_id: "io.gitlab.liferooter.Compendium"
            );
        }

        static construct {
            settings = new GLib.Settings ("io.gitlab.liferooter.Compendium");
        }
        protected override void activate () {
            Sourceinit ();

            setup_theme ();
            setup_accels ();

            var win = new Comp.MainWindow (this);
            win.present ();
        }

        void setup_theme () {
            style_manager.set_color_scheme (ColorScheme.FORCE_LIGHT);
        }

        void setup_accels () {
            add_action_entries (ACTIONS, this);

            foreach (var accel in ACCELS)
                set_accels_for_action (accel.action_name, accel.accels);
        }

        void action_quit () {
            quit ();
        }

        static int main (string[] argv) {
            return new Application ().run (argv);
        }
    }
}
