using Gee, Gtk;

namespace Comp {
    public struct Range {
        int start;
        int end;
    }

    public class Line : Object {
        public string text { get; set; }

        public HashMap<string, ArrayList<Range?>> formatting { get; set; }

        public Line (string text = "") {
            Object (
                text: text
            );
        }

        public void apply_formatting (MarkdownBuffer buffer, int line_index) {
            buffer.begin_internal_action ();

            TextIter cursor;
            buffer.get_iter_at_mark (out cursor, buffer.get_insert ());

            int cursor_pos = -1;
            if (cursor.get_line () == line_index) {
                cursor_pos = buffer.cursor_position;
            }
            TextIter start_iter, end_iter;
            buffer.get_iter_at_line (out start_iter, line_index);
            buffer.get_iter_at_line (out end_iter, line_index + 1);
            buffer.delete (ref start_iter, ref end_iter);
            buffer.insert (ref start_iter, text, text.length);
            if (cursor_pos != -1) {
                buffer.get_iter_at_offset (out cursor, cursor_pos);
                buffer.place_cursor (cursor);
            }

            buffer.end_internal_action ();

            foreach (var style_name in formatting.keys) {
                var style = buffer.get_style (style_name);

                int start, end;

                foreach (var range in formatting[style_name]) {
                    if (range == null) {
                        start = 0;
                        end = text.length;
                    } else {
                        start = ((!) range).start;
                        end = ((!) range).end;
                    }
                    style?.apply (buffer, line_index, start, end);
                }
            }
        }
    }
}