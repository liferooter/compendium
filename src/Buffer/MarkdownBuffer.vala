using Gtk, Gee;

namespace Comp {

    public class MarkdownBuffer : SourceBuffer {
        public string raw_text {
            owned get {
                TextIter start, end;
                get_bounds (out start, out end);
                return get_text (start, end, true);
            } set {
                set_text (value, value.length);
            }
        }

        private int internal_action_level = 0;

        private HashMap<string, Markdown.TextStyle> styles;

        public ArrayList<Line> lines = new ArrayList<Line>.wrap ({
            new Line ()
        });

        public MarkdownBuffer (TextTagTable tags) {
            Object (
                tag_table: tags,
                enable_undo: false
            );
        }

        construct {
            insert_text.connect (insert_text_cb);
            delete_range.connect (delete_text_cb);
        }

        void insert_text_cb (ref TextIter pos, string new_text, int new_text_length) {
            if (internal_action_level != 0)
                return;

            var new_lines = new_text.split ("\n");
            var start_line_number = pos.get_line ();

            string text_after_cursor;

            with (lines.get (start_line_number)) {
                text_after_cursor = text[pos.get_line_index ():];
                text = text[:pos.get_line_index()];
            }

            for (var i = 0; i < new_lines.length; i++) {
                if (i != 0) {
                    lines.insert (start_line_number + i, new Line ());
                }
                lines[start_line_number + i].text += new_lines[i];
            }
            lines[start_line_number + new_lines.length - 1].text += text_after_cursor;
        }

        void delete_text_cb (TextIter start, TextIter end) {
            if (internal_action_level != 0)
                return;

            var start_line_num = start.get_line ();
            var end_line_num = end.get_line ();

            var start_line = lines[start_line_num];
            var end_line = lines[end_line_num];

            start_line.text = start_line.text[:start.get_line_index ()] + end_line.text[end.get_line_index ():];

            for (int i = end_line_num; i > start_line_num; i--) {
                lines.remove_at (i);
            }
        }

        public void register_style (Markdown.TextStyle style, string name) {
            if (styles.has_key (name))
                return;
            styles[name] = style;
        }

        public Markdown.TextStyle? get_style (string name) {
            if (styles.has_key (name))
                return styles[name];
            else
                return null;
        }

        internal void begin_internal_action () {
            begin_irreversible_action ();
            internal_action_level++;
        }

        internal void end_internal_action () {
            assert (internal_action_level != 0);
            internal_action_level--;
            end_irreversible_action ();
        }
    }
}