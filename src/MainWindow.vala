namespace Comp {
    [GtkTemplate (ui = "/io/gitlab/liferooter/Compendium/ui/Window.ui")]
    class MainWindow : Adw.ApplicationWindow {

        [GtkChild] unowned Comp.TextView editor;

        public MainWindow (Comp.Application application) {
            Object (
                application: application
            );
        }

        construct {
            editor.setup_actions (this);
        }
    }
}
