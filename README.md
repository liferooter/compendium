# Compendium [WIP]
Markdown-based knowledge database.
# TODO
## Markdown syntax support
- [x] Headers
- [x] Bold
- [x] Italic
- [x] Strikethrough
- [ ] Lists
    - [ ] Unordered
    - [x] Ordered
    - [ ] Nested
- [ ] Images
- [x] Links
    - [x] Auto links
    - [x] Links with label
- [ ] Blockquotes
- [ ] Code
- [ ] Tables
- [ ] Task lists
## Editor features
- [ ] Save file
- [ ] Open file
- [ ] Toggle editing
## App features
- [ ] Document structure
- [ ] Structured database
- [ ] Export file as
    - [ ] PDF
    - [ ] HTML
- [ ] Export database
# Build
## Meson
```bash
git clone https://gitlab.com/liferooter/compendium
meson _build -Dprefix=INSTALLATION_PREFIX
ninja -C _build
sudo ninja -C _build install
```

